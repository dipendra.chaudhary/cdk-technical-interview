const AWS = require('aws-sdk');

exports.handler = async (event, context) => {
  const ec2 = new AWS.EC2();
  const instances = await ec2.describeInstances().promise();

  const instancesToDelete = instances.Reservations.flatMap((reservation) =>
    reservation.Instances.filter((instance) => {
      const nameTag = instance.Tags.find((tag) => tag.Key === 'Name');
      return nameTag && nameTag.Value === event.instanceName; // Use instanceName from event
    })
  );

  const deletionPromises = instancesToDelete.map((instance) =>
    ec2.terminateInstances({ InstanceIds: [instance.InstanceId] }).promise()
  );

  await Promise.all(deletionPromises);

  return {
    statusCode: 200,
    body: JSON.stringify({
      message: `Instances with tag Name: ${event.instanceName} deleted successfully.`,
    }),
  };
};