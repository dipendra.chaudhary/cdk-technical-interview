const AWS = require('aws-sdk');

async function testLambda() {
  const lambda = new AWS.Lambda({ region: 'us-east-1' });

  const params = {
    FunctionName: 'Ec2InstanceInfoLambda', // Replace with the actual Lambda function name
    InvocationType: 'RequestResponse',
    LogType: 'Tail',
    Payload: '{}', // Add any input payload if required
  };

  try {
    const response = await lambda.invoke(params).promise();

    const data = JSON.parse(response.Payload);
    console.log('Instance Info:', data);
  } catch (error) {
    console.error('Error:', error);
  }
}

testLambda();