import * as cdk from 'aws-cdk-lib';
import { Stack } from 'aws-cdk-lib';
import {Construct} from 'constructs';
import {Template} from 'aws-cdk-lib/assertions';
import { LambdaFunction } from 'aws-cdk-lib/aws-events-targets';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import {LambdaStack} from "../lib/ec2InstanceTagsInfoLambda";

test('Stack should have 1 lambda', () => {
    const app = new cdk.App();
    const stack = new LambdaStack(app, 'test-stack');

    const template = Template.fromStack(stack);

    template.resourceCountIs('AWS::Lambda::Function', 1)
});

test('Stack should have Lambda Function called Ec2InstanceInfoLambda, Runtime: nodejs14.x and Handler: index.handler', () => {
    const app = new cdk.App();
    const stack = new LambdaStack(app, 'test-stack');

    const template = Template.fromStack(stack);

    template.hasResourceProperties('AWS::Lambda::Function' ,{
      Handler: 'index.handler',
      Runtime: 'nodejs14.x',
      FunctionName: 'Ec2InstanceInfoLambda'
    })
})

test('Stack should have access to Describe EC2 Instance policy attached to the Lambda Function', () => {
    const app = new cdk.App();
    const stack = new LambdaStack(app, 'test-stack');

    const template = Template.fromStack(stack);

    template.hasResourceProperties('AWS::IAM::Policy', {
      PolicyDocument: {
        Statement: [
          {
            Action: 'ec2:DescribeInstances',
            Effect: 'Allow',
            Resource: '*',
          },
        ],
        Version: '2012-10-17',
      },
    })
});

// test('Stack should have EC2 instance called AutomatedTestingInstance and tagged with department', () => {
//     const app = new cdk.App();
//     const stack = new SurecloudCdkScenarioPrerequisiteStack(app, 'test-stack');

//     const template = Template.fromStack(stack);

//     template.hasResourceProperties('AWS::EC2::Instance', {
//         Tags: [{Key: 'department', Value: 'development'},
//             {Key: 'Name', Value: 'AutomatedTestingInstance'}]
//     });
// })
