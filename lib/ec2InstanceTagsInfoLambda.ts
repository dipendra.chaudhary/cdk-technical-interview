import {Stack, StackProps, Tags, CfnOutput} from 'aws-cdk-lib';
import {Construct} from 'constructs';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as ec2 from 'aws-cdk-lib/aws-ec2';
import * as iam from 'aws-cdk-lib/aws-iam'

export class LambdaStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const handler = new lambda.Function(this, 'Ec2InstanceInfoLambda', {
        functionName: 'Ec2InstanceInfoLambda',
        runtime: lambda.Runtime.NODEJS_14_X,
        code: lambda.Code.fromAsset('lambda'),
        handler: 'index.handler',
      });

    handler.addToRolePolicy(new iam.PolicyStatement({
      actions: ['ec2:DescribeInstances', 'ec2:TerminateInstances'],
      resources: ['*'],
    }));

    new CfnOutput(this, 'Ec2InstanceInfoLambdaArn', {
      value: handler.functionArn,
      description: 'ARN of the Lambda function that returns EC2 instance information',
    });
  }
}
