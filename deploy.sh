#!/bin/bash

# Install dependencies (if needed)
echo Installing Dependencies
npm install

# Bootstrap the AWS environment (if needed)
echo Bootstraping the AWS environment
cdk bootstrap

# Deploy all the CDK stack
echo Running CDk Deploy for all the stacks
cdk deploy --all --require-approval=never

# Test CDK Usage
echo running npm test
npm test

# Get tags Usage
echo getting ec2 Name and tags
node lambda-tags/get-tags.js