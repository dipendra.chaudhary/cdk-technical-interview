# Technical Assessment Done by Dipendra Chaudhary

<details open="open">
  <summary><h1> Table of Contents</h1></summary>
  <ol>
    <li>
      <a href="#introduction">Introduction</a>
    </li>
    <li>
      <a href="#commands-in-summary">Commands in summary</a>
    </li>
    <li>
      <a href="#surecloud-cdk-scenario-prerequisite-stack">Sure Cloud CDK Scenario Prequisite Stack</a>
      <ul>
        <li><a href="#surecloud-ec2-cdk-usage">EC2 CDK Usage</a></li>
      </ul>
    </li>
    <li>
      <a href="#ec2-instance-tags-info-lambda">EC2 Instance Tags Info Lambda</a>
      <ul>
        <li><a href="#lambda-cdk-usage">Lamds CDK Usage</a></li>
      </ul>
    <li>
      <a href="#test-coverage">Test Coverage</a>
      <ul>
        <li><a href="#test-cdk-usage">Test CDK Usage</a></li>
      </ul>
    </li>
    <li>
      <a href="#get-tags">Get Tags</a>
      <ul>
        <li><a href="#get-tags-usage">Get tags Usage</a></li>
      </ul>
    </li>
    <li>
      <a href="#deploy-simple">Deploy Simple</a>
    </li>
    <li>
      <a href="#destroy-simple">Destroy Simple</a>
    </li>
  </ol>
</details>

# Introduction

SureCloud has 3 EC2 instances which are assigned to different parts of the business. We can know which instances belong to which areas of the business by the help of Lambda function so that specific actions can be performed.

## Commands in Summary

* `npm install` installs the dependencies required for the app

* `npm run build`   compile typescript to js

* `npm run watch`   watch for changes and compile

* `npm run test`    perform the jest unit tests

* `cdk deploy`      deploy the stack

* `cdk diff`        compare deployed stacks with current state

* `cdk synth`       emits the synthesized CloudFormation template

* `cdk destroy`     destroy the stack

## SureCloud CDK scenario prerequisite stack

This stack contains the cdk codes to create 3 EC2 instances that belongs to  the different areas ofth business

### SureCloud EC2 CDK Usage

* `cdk ls`          list the stack

* `cdk diff surecloud-cdk-scenario-prerequisite-stack` compare deployed stacks with current state of surecloud-cdk-scenario-prerequisite-stack

* `cdk deploy surecloud-cdk-scenario-prerequisite-stack` deploy the cdk deploy surecloud-cdk-scenario-prerequisite-stack

* `cdk destroy surecloud-cdk-scenario-prerequisite-stack` destroy the cdk deploy surecloud-cdk-scenario-prerequisite-stack

## EC2 Instance Tags Info Lambda

This stack contains the Lambda code that gets the Name and tags of the EC2 Instances.

### Lamds CDK Usage

* `cdk ls`          list the stack

* `cdk diff ec2InstanceTagsInfoLambda` compare deployed stacks with current state of ec2InstanceTagsInfoLambda

* `cdk deploy ec2InstanceTagsInfoLambda` deploy the cdk deploy ec2InstanceTagsInfoLambda

* `cdk destroy ec2InstanceTagsInfoLambda` destroy the cdk deploy ec2InstanceTagsInfoLambda

# Test Coverage

Test coverage is a technique where our test cases cover application code and on specific conditions those test cases are met.

## Test CDK Usage

* `npm test`  npm is running the test command as defined in the package. json configuration file

# Get Tags

We can check if the Lambda function is getting the Name and tags of instance

## Get tags Usage

* `node lambda-tags/get-tags.js`  Get the Name and tags of the EC2 instance

# Deploy Simple

* `chmod +x deploy.sh`
* `./deploy.sh`

# Destroy Simple

* `chmod +x destroy.sh`
* `./destroy.sh`